package com.example.kananyusub.wallhance.listener;

import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;

public abstract class PaginationScrollListener extends RecyclerView.OnScrollListener {

    private int mVisibleThreshold = 1;

    private StaggeredGridLayoutManager mLayoutManager;


    public PaginationScrollListener(StaggeredGridLayoutManager layoutManager) {
        this.mLayoutManager = layoutManager;
        mVisibleThreshold = mVisibleThreshold * layoutManager.getSpanCount();
    }

    public int getLastVisibleItem(int[] lastVisibleItemPositions) {
        int maxSize = 0;
        for (int i = 0; i < lastVisibleItemPositions.length; i++) {
            if (i == 0) {
                maxSize = lastVisibleItemPositions[i];
            } else if (lastVisibleItemPositions[i] > maxSize) {
                maxSize = lastVisibleItemPositions[i];
            }
        }
        return maxSize;
    }

    @Override
    public void onScrolled(RecyclerView view, int dx, int dy) {

        int lastVisibleItemPosition = 0;
        int totalItemCount = mLayoutManager.getItemCount();
        int[] lastVisibleItemPositions = mLayoutManager.findLastVisibleItemPositions(null);

        lastVisibleItemPosition = getLastVisibleItem(lastVisibleItemPositions);


        if (!isLoading() && !isLastPage()) {

            if (lastVisibleItemPosition + mVisibleThreshold > totalItemCount
                    && view.getAdapter().getItemCount() > mVisibleThreshold) {
                loadMoreItems();
            }

        }
    }

    protected abstract void loadMoreItems();

    public abstract int getTotalPageCount();

    public abstract boolean isLastPage();

    public abstract boolean isLoading();
}
