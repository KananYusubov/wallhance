package com.example.kananyusub.wallhance.pojo;

import com.google.gson.annotations.SerializedName;

public class ImageUrl {

    @SerializedName("full")
    private String mFullImageUrl;

    @SerializedName("small")
    private String mSmallImageUrl;

    @SerializedName("thumb")
    private String mThumbImageUrl;

    public String getSmallImageUrl() {
        return mSmallImageUrl;
    }

    public String getThumbImageUrl() {
        return mThumbImageUrl;
    }

    public String getFullImageUrl() {
        return mFullImageUrl;
    }
}
