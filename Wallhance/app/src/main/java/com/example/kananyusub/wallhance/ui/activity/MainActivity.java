package com.example.kananyusub.wallhance.ui.activity;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.kananyusub.wallhance.R;
import com.example.kananyusub.wallhance.api.ApiInitHelper;
import com.example.kananyusub.wallhance.listener.PaginationScrollListener;
import com.example.kananyusub.wallhance.pojo.ApiResponse;
import com.example.kananyusub.wallhance.pojo.SearchResponse;
import com.example.kananyusub.wallhance.ui.adapter.ImageAdapter;
import com.example.kananyusub.wallhance.ui.adapter.ImageViewHolder;
import com.example.kananyusub.wallhance.util.Constant;
import com.example.kananyusub.wallhance.util.Util;
import com.facebook.shimmer.ShimmerFrameLayout;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeoutException;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class MainActivity extends AppCompatActivity
        implements ImageViewHolder.ListItemClickListener,
        WallPaperAsyncTask.OnWallPaperSuccessListener {

    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.edittext_search)
    EditText mEditTextSearch;
    @BindView(R.id.recycler_images)
    RecyclerView mRecyclerView;
    @BindView(R.id.shimmer_view_layout)
    ShimmerFrameLayout mShimmerFrameLayout;
    @BindView(R.id.progress_loading)
    ProgressBar mProgressBar;
    @BindView(R.id.image_no_internet)
    ImageView mImageViewNoInternet;
    @BindView(R.id.swipe_refresh)
    SwipeRefreshLayout mRefreshLayout;
    private List<ApiResponse> mImageList;
    private ImageAdapter mAdapter;
    private StaggeredGridLayoutManager mStaggeredGridLayoutManager;
    private static final int PAGE_START = 1;
    private boolean mIsLoading = false;
    private static final int TOTAL_PAGES = 5;
    private int mCurrentPage = PAGE_START;
    private boolean mIsLastPage = false;
    private boolean mIsSearched = false;
    private String mCurrentQuery;
    private Timer timer;
    private FullImageDialog mFullImageDialog;

    @Override
    protected void onStart() {
        super.onStart();
        mShimmerFrameLayout.startShimmer();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.partial_toolbar);

        mEditTextSearch.addTextChangedListener(searchTextWatcher);

        if (Util.isOnline(this)) {
            initRecyclerView();
            getFirstPage();
        } else {
            if (mShimmerFrameLayout != null) {
                mShimmerFrameLayout.stopShimmer();
                mShimmerFrameLayout.setVisibility(View.GONE);
            }
            mImageViewNoInternet.setVisibility(View.VISIBLE);
            mImageViewNoInternet.setImageResource(R.drawable.ic_no_internet);
        }

        mRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (mAdapter != null) {
                    mAdapter.clear();
                }
                if (Util.isOnline(MainActivity.this)) {
                    mImageViewNoInternet.setVisibility(View.GONE);
                    if (mEditTextSearch.getText().length() == 0) {
                        getFirstPage();
                    } else {
                        mEditTextSearch.getText().clear();
                    }
                } else {
                    mImageViewNoInternet.setVisibility(View.VISIBLE);
                    mImageViewNoInternet.setImageResource(R.drawable.ic_no_internet);
                }

                mRefreshLayout.setRefreshing(false);
            }
        });


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mShimmerFrameLayout.stopShimmer();
    }

    @Override
    public void onListItemClick(int position) {
        if (mAdapter.getImageByPosition(position) != null) {
            openFullImageDialog(position);
        }
    }

    private void getFirstPage() {
        ApiInitHelper.getInstance().getApi().getRandomPhotos(
                Constant.ACCESS_KEY,
                1,
                10
        )
                .enqueue(new Callback<List<ApiResponse>>() {
                    @Override
                    public void onResponse(Call<List<ApiResponse>> call, Response<List<ApiResponse>> response) {

                        if (!response.isSuccessful()) {
                            Toast.makeText(MainActivity.this, "CODE: " + response.code(),
                                    Toast.LENGTH_SHORT).show();
                            return;
                        }

                        mCurrentPage = 1;
                        mIsLastPage = false;
                        mIsLoading = false;

                        mImageList = response.body();
                        mAdapter.addAll(mImageList);
                        mShimmerFrameLayout.stopShimmer();
                        mShimmerFrameLayout.setVisibility(View.GONE);

                        if (mCurrentPage <= TOTAL_PAGES && response.body().size() >= 10)
                            mAdapter.addLoadingFooter();
                        else mIsLastPage = true;
                    }

                    @Override
                    public void onFailure(Call<List<ApiResponse>> call, Throwable t) {
                        t.printStackTrace();
                        Toast.makeText(MainActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void initRecyclerView() {
        mStaggeredGridLayoutManager = new StaggeredGridLayoutManager(2,
                LinearLayoutManager.VERTICAL);
        mRecyclerView.setLayoutManager(mStaggeredGridLayoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mImageList = new ArrayList<>();
        mAdapter = new ImageAdapter(this, mImageList);
        mRecyclerView.setAdapter(mAdapter);

        mRecyclerView.addOnScrollListener(new PaginationScrollListener(mStaggeredGridLayoutManager) {
            @Override
            protected void loadMoreItems() {
                mIsLoading = true;
                mCurrentPage++;

                new Handler().postDelayed(
                        new Runnable() {
                            @Override
                            public void run() {
                                loadNextPage(mCurrentPage);
                            }
                        }, 2000
                );
                // load Next Page

            }

            @Override
            public int getTotalPageCount() {
                return TOTAL_PAGES;
            }

            @Override
            public boolean isLastPage() {
                return mIsLastPage;
            }

            @Override
            public boolean isLoading() {
                return mIsLoading;
            }
        });
    }

    private void openFullImageDialog(int position) {
        mFullImageDialog = new FullImageDialog();
        mFullImageDialog.setApiResponse(
                mAdapter.getImageByPosition(position)
        );

        mFullImageDialog.show(getSupportFragmentManager(), "full image dialog");
    }

    private void loadNextPage(int page) {

        if (mIsSearched) {
            loadNextSearchPage();
        } else {
            ApiInitHelper.getInstance().getApi().getRandomPhotos(
                    Constant.ACCESS_KEY,
                    page,
                    10)
                    .enqueue(new Callback<List<ApiResponse>>() {
                        @Override
                        public void onResponse(Call<List<ApiResponse>> call, Response<List<ApiResponse>> response) {

                            mAdapter.removeLoadingFooter();
                            mIsLoading = false;

                            if (!response.isSuccessful()) {
                                Toast.makeText(MainActivity.this, "CODE: " + response.code(),
                                        Toast.LENGTH_SHORT).show();
                                return;
                            }

                            mAdapter.addAll(response.body());
                            if (mCurrentPage <= TOTAL_PAGES && response.body().size() >= 10)
                                mAdapter.addLoadingFooter();
                            else mIsLastPage = true;
                        }

                        @Override
                        public void onFailure(Call<List<ApiResponse>> call, Throwable t) {
                            Toast.makeText(MainActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    });
        }
    }

    private void search(String query) {
        mCurrentQuery = query;
        ApiInitHelper.getInstance()
                .getApi()
                .search(Constant.ACCESS_KEY,
                        1,
                        query,
                        10).enqueue(new Callback<SearchResponse>() {
            @Override
            public void onResponse(Call<SearchResponse> call, Response<SearchResponse> response) {

                if (!response.isSuccessful()) {
                    Toast.makeText(MainActivity.this, "CODE: " + response.code(),
                            Toast.LENGTH_SHORT).show();
                    return;
                }

                if (response.body().getResults() != null) {
                    if (response.body().getResults().size() == 0) {
                        mImageViewNoInternet.setVisibility(View.VISIBLE);
                        mImageViewNoInternet.setImageResource(R.drawable.ic_not_found);
                        mCurrentPage = 1;
                        mIsLastPage = false;
                        mIsLoading = false;
                        return;
                    }
                }

                mCurrentPage = 1;
                mIsLastPage = false;
                mIsLoading = false;

                mAdapter.setSearchResult(response.body().getResults());

                if (mCurrentPage <= TOTAL_PAGES && response.body().getResults().size() >= 10)
                    mAdapter.addLoadingFooter();
                else mIsLastPage = true;
            }

            @Override
            public void onFailure(Call<SearchResponse> call, Throwable t) {
                Toast.makeText(MainActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void showProgressbar(boolean show) {
        if (show) {
            mProgressBar.setVisibility(View.VISIBLE);
        } else {
            mProgressBar.setVisibility(View.INVISIBLE);
        }
    }

    private void loadNextSearchPage() {
        ApiInitHelper.getInstance()
                .getApi()
                .search(Constant.ACCESS_KEY,
                        mCurrentPage,
                        mCurrentQuery,
                        10)
                .enqueue(new Callback<SearchResponse>() {
                    @Override
                    public void onResponse(Call<SearchResponse> call, Response<SearchResponse> response) {

                        mAdapter.removeLoadingFooter();
                        mIsLoading = false;

                        if (!response.isSuccessful()) {
                            Toast.makeText(MainActivity.this, "CODE: " + response.code(),
                                    Toast.LENGTH_SHORT).show();
                            return;
                        }


                        if (response.body().getResults() != null) {
                            if (response.body().getResults().size() == 0) {
                                mImageViewNoInternet.setVisibility(View.VISIBLE);
                                mImageViewNoInternet.setImageResource(R.drawable.ic_not_found);
                                mCurrentPage = 1;
                                mIsLastPage = false;
                                mIsLoading = false;
                                return;
                            }
                        }

                        mAdapter.addAll(response.body().getResults());

                        if (mCurrentPage <= TOTAL_PAGES && response.body().getResults().size() >= 10)
                            mAdapter.addLoadingFooter();
                        else mIsLastPage = true;
                    }

                    @Override
                    public void onFailure(Call<SearchResponse> call, Throwable t) {
                        Toast.makeText(MainActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                        mAdapter.removeLoadingFooter();
                        mIsLoading = false;
                        mCurrentPage--;
                    }
                });
    }

    private TextWatcher searchTextWatcher = new TextWatcher() {
        @Override
        public void afterTextChanged(Editable arg0) {

            timer = new Timer();
            timer.schedule(new TimerTask() {
                @Override
                public void run() {

                    MainActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            mImageViewNoInternet.setVisibility(View.GONE);
                            showProgressbar(true);
                            mAdapter.clear();
                        }
                    });

                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    MainActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            showProgressbar(false);
                            if (arg0.length() > 0) {
                                mIsSearched = true;
                                mIsLastPage = false;
                                mCurrentPage = 1;
                                mIsLoading = false;
                                search(arg0.toString());
                            } else {
                                mIsSearched = false;
                                getFirstPage();
                            }
                        }
                    });

                }
            }, 1000);
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if (timer != null) {
                showProgressbar(false);
                timer.cancel();
            }
        }
    };

    @Override
    public void onSuccess() {
        if (mFullImageDialog != null) {
            mFullImageDialog.dismiss();
        }
    }
}
