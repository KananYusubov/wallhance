package com.example.kananyusub.wallhance.api;

import com.example.kananyusub.wallhance.pojo.ApiResponse;
import com.example.kananyusub.wallhance.pojo.SearchResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ApiMethods {

    String CLIENT_ID = "client_id";
    String PAGE = "page";
    String PER_PAGE = "per_page";
    String QUERY = "query";

    @GET("/photos/")
    Call<List<ApiResponse>> getRandomPhotos(
            @Query(CLIENT_ID) String clientId,
            @Query(PAGE) int page,
            @Query(PER_PAGE) int perPage
    );

    @GET("/search/photos/")
    Call<SearchResponse> search(
            @Query(CLIENT_ID) String clientId,
            @Query(PAGE) int page,
            @Query(QUERY) String query,
            @Query(PER_PAGE) int perPage
    );
}
