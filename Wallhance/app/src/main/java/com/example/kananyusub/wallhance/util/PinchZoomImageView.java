package com.example.kananyusub.wallhance.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.support.v7.widget.AppCompatImageView;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;

public class PinchZoomImageView extends AppCompatImageView {

    private float whRatio = 0;
    private final static float MIN_ZOOM = 1.f;
    private final static float MAX_ZOOM = 3.f;
    private float mScaleFactor = 1.f;
    private ScaleGestureDetector mScaleGestureDetector;
    private Bitmap mBitmap;


    private class ScaleListener extends ScaleGestureDetector.SimpleOnScaleGestureListener {

        @Override
        public boolean onScale(ScaleGestureDetector detector) {
            mScaleFactor *= detector.getScaleFactor();
            mScaleFactor = Math.max(MIN_ZOOM, Math.min(MAX_ZOOM, mScaleFactor));
            invalidate();
            requestLayout();
            return super.onScale(detector);
        }
    }

    public PinchZoomImageView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        mScaleGestureDetector = new ScaleGestureDetector(getContext(), new ScaleListener());
    }

    public PinchZoomImageView(Context context) {
        super(context);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        if (whRatio != 0) {
            int width = getMeasuredWidth();
            int height = (int) (whRatio * width);

            int scaledWidth = Math.round(width * mScaleFactor);
            int scaledHeight = Math.round(height * mScaleFactor);


            setMeasuredDimension(
                    Math.min(width, width),
                    Math.min(height, height));
        }
    }

    public void setWhRatio(float whRatio) {
        this.whRatio = whRatio;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.save();
        canvas.scale(mScaleFactor,
                mScaleFactor,
                mScaleGestureDetector.getFocusX(),
                mScaleGestureDetector.getFocusY());

        if (mBitmap != null) {
            canvas.drawBitmap(mBitmap, 0, 0, null);
        }
        canvas.restore();
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        mScaleGestureDetector.onTouchEvent(event);
        return true;
    }

    public void setImageBitmap(Bitmap bitmap) {
        mBitmap = bitmap;
        invalidate();
        requestLayout();
    }
}
