package com.example.kananyusub.wallhance.ui.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;

import com.example.kananyusub.wallhance.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FooterHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.progress_loading)
    ProgressBar mProgressBar;

    public FooterHolder(@NonNull View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }
}
