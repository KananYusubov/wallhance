package com.example.kananyusub.wallhance.ui.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.kananyusub.wallhance.R;
import com.example.kananyusub.wallhance.pojo.ApiResponse;

import java.util.List;

public class ImageAdapter extends RecyclerView.Adapter {

    private List<ApiResponse> mImageList;
    private Context mContext;
    private static final int VIEW_NORMAL = 0;
    private static final int VIEW_LOADING = 1;

    //flag for Footer ProgressBar
    private boolean isLoadingAdded = false;

    public ImageAdapter(Context context, List<ApiResponse> iamgeList) {
        mImageList = iamgeList;
        mContext = context;
    }

    @Override
    public int getItemViewType(int position) {
        return (position == mImageList.size() - 1
                && isLoadingAdded) ? VIEW_LOADING : VIEW_NORMAL;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view;
        switch (i) {
            case VIEW_NORMAL:
                view = LayoutInflater.from(mContext)
                        .inflate(R.layout.item_image, viewGroup, false);
                return new ImageViewHolder(mContext, view, mImageList);

            case VIEW_LOADING:
                view = LayoutInflater.from(mContext)
                        .inflate(R.layout.item_loading, viewGroup, false);
                return new FooterHolder(view);
        }

        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        switch (getItemViewType(i)) {
            case VIEW_NORMAL:
                ImageViewHolder holder = (ImageViewHolder) viewHolder;
                holder.bind(i);
                break;

            case VIEW_LOADING:
                break;
        }

    }

    @Override
    public int getItemCount() {
        return mImageList == null ? 0 : mImageList.size();
    }

    public void add(ApiResponse data) {
        float ratio = (float) data.getHeight() / (float) data.getWidth();
        data.setRatio(ratio);
        mImageList.add(data);
        notifyItemInserted(mImageList.size() - 1);
    }

    public void setSearchResult(List<ApiResponse> newResult){
        clear();
        addAll(newResult);
        notifyDataSetChanged();
    }

    public void addAll(List<ApiResponse> imageList) {
        for (ApiResponse response : imageList) {
            add(response);
        }
    }

    public void remove(ApiResponse apiResponse) {
        int positon = mImageList.indexOf(apiResponse);
        if (positon > -1) {
            mImageList.remove(positon);
            notifyItemRemoved(positon);
        }
    }

    public void clear() {
        isLoadingAdded = false;
        while (getItemCount() > 0) {
            remove(getImageByPosition(0));
        }
    }

    public boolean isEmpty() {
        return getItemCount() == 0;
    }

    public void addLoadingFooter() {
        isLoadingAdded = true;
        add(new ApiResponse());
    }

    public void removeLoadingFooter() {
        isLoadingAdded = false;

        int position = mImageList.size() - 1;
        ApiResponse apiResponse = mImageList.get(position);
        if (apiResponse != null) {
            mImageList.remove(position);
            notifyItemRemoved(position);
        }
    }

    public ApiResponse getImageByPosition(int position) {
        return mImageList.get(position);
    }
}
