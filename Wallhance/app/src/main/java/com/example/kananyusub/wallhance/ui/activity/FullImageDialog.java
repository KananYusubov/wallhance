package com.example.kananyusub.wallhance.ui.activity;

import android.app.Dialog;
import android.app.WallpaperManager;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatDialogFragment;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.example.kananyusub.wallhance.R;
import com.example.kananyusub.wallhance.pojo.ApiResponse;
import com.example.kananyusub.wallhance.util.DynamicHeightImageView;
import com.example.kananyusub.wallhance.util.PinchZoomImageView;

import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.net.URL;
import java.util.concurrent.ExecutionException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class FullImageDialog extends AppCompatDialogFragment
        implements WallPaperAsyncTask.OnWallPaperSuccessListener {

    private static ApiResponse mApiResponse;
    @BindView(R.id.image_full)
    DynamicHeightImageView mImageViewFull;
    @BindView(R.id.progress_loading)
    ProgressBar mProgressBar;
    @BindView(R.id.button_set_wallpaper)
    Button mButtonSetWallpaper;

    @OnClick(R.id.button_cancel)
    public void cancel() {
        getDialog().dismiss();
    }


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_full_image, null);

        ButterKnife.bind(this, view);

        RelativeLayout.LayoutParams rlp = (RelativeLayout.LayoutParams) mImageViewFull.getLayoutParams();
        float ratio = (float) mApiResponse.getHeight() / (float) mApiResponse.getWidth();
        rlp.height = (int) (rlp.width * ratio);
        mImageViewFull.setLayoutParams(rlp);
        mImageViewFull.setWhRatio(mApiResponse.getRatio());

        RequestOptions requestOptions = new RequestOptions()
                .placeholder(R.drawable.drawable_placeholder);

        Glide.with(getActivity())
                .load(mApiResponse.getUrls().getFullImageUrl())
                .apply(requestOptions)
                .thumbnail(Glide.with(getActivity())
                        .load(mApiResponse.getUrls().getThumbImageUrl())
                        .apply(requestOptions))
                .into(mImageViewFull);

        builder.setView(view);

        AlertDialog alertDialog = builder.create();

        alertDialog.setCancelable(false);

        mButtonSetWallpaper.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                WallPaperAsyncTask task = new WallPaperAsyncTask(getActivity(), mProgressBar, mApiResponse);
                task.execute();
            }
        });

        return alertDialog;
    }

    public void setApiResponse(ApiResponse apiResponse) {
        mApiResponse = apiResponse;
    }

    @Override
    public void onSuccess() {
        Toast.makeText(getActivity(), getString(R.string.msg_wallpaper_success), Toast.LENGTH_SHORT).show();
    }
}


class WallPaperAsyncTask extends AsyncTask<Void, Void, Boolean> {
    private boolean mIsSuccessful = true;
    private WeakReference<ProgressBar> mProgressBarWeakReference;
    private Context mContext;
    private OnWallPaperSuccessListener mListener;
    private ApiResponse mApiResponse;
    private static WallpaperManager wpm;

    WallPaperAsyncTask(Context context, ProgressBar progressBar, ApiResponse apiResponse) {
        mProgressBarWeakReference = new WeakReference<>(progressBar);
        mContext = context;
        mApiResponse = apiResponse;
        try {
            mListener = (OnWallPaperSuccessListener) context;
        } catch (ClassCastException e) {
            Log.e("ERROR", "CLASS_CAST_EXCEPTION");
        }
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        ProgressBar progressBar = mProgressBarWeakReference.get();
        if (progressBar != null) {
            progressBar.setVisibility(View.VISIBLE);
        } else {
            return;
        }
    }

    @Override
    protected Boolean doInBackground(Void... voids) {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        InputStream ins = null;
        wpm = WallpaperManager.getInstance(mContext);
        try {
            ins = new URL(mApiResponse.getUrls().getFullImageUrl())
                    .openStream();
            wpm.setStream(ins);
        } catch (IOException e) {
            e.printStackTrace();
            mIsSuccessful = false;
        }
        return mIsSuccessful;
    }

    @Override
    protected void onPostExecute(Boolean aBoolean) {
        super.onPostExecute(aBoolean);

        ProgressBar progressBar = mProgressBarWeakReference.get();
        if (progressBar != null) {
            progressBar.setVisibility(View.GONE);

            if (aBoolean) {
                mListener.onSuccess();
                Toast.makeText(mContext,
                        "SUCCESS!",
                        Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(mContext,
                        "FAILURE!",
                        Toast.LENGTH_SHORT).show();
            }

        } else {
            return;
        }

    }

    public interface OnWallPaperSuccessListener {
        public void onSuccess();
    }
}