package com.example.kananyusub.wallhance.util;

import android.content.Context;
import android.support.v7.widget.AppCompatImageView;
import android.util.AttributeSet;

public class DynamicHeightImageView extends AppCompatImageView {

    private float whRatio = 0;

    public DynamicHeightImageView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public DynamicHeightImageView(Context context) {
        super(context);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        if(whRatio!=0){
            int width = getMeasuredWidth();
            int height = (int) (whRatio*width);
            setMeasuredDimension(width, height);
        }
    }

    public void setWhRatio(float whRatio) {
        this.whRatio = whRatio;
    }


}
