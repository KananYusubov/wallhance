package com.example.kananyusub.wallhance.ui.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.bumptech.glide.request.RequestOptions;
import com.example.kananyusub.wallhance.R;
import com.example.kananyusub.wallhance.pojo.ApiResponse;
import com.example.kananyusub.wallhance.util.DynamicHeightImageView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ImageViewHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.image_dynamic)
    DynamicHeightImageView mImageViewPic;
    private List<ApiResponse> mImageList;
    private Context mContext;
    private ListItemClickListener mCallBack;

    public ImageViewHolder(Context context, @NonNull View itemView, List<ApiResponse> imageList) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        mContext = context;
        mImageList = imageList;

        try {
            mCallBack = (ListItemClickListener) mContext;
        } catch (ClassCastException e) {
            Toast.makeText(context,
                    "There is an error in your Holder",
                    Toast.LENGTH_SHORT).show();
        }

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getAdapterPosition() != RecyclerView.NO_POSITION) {
                    int clickedItemPosition = getAdapterPosition();
                    mCallBack.onListItemClick(clickedItemPosition);
                }
            }
        });
    }


    public void bind(int position) {

        ApiResponse currentData = mImageList.get(position);

        RelativeLayout.LayoutParams rlp = (RelativeLayout.LayoutParams) mImageViewPic.getLayoutParams();
        float ratio = (float) currentData.getHeight() / (float) currentData.getWidth();
        rlp.height = (int) (rlp.width * ratio);
        mImageViewPic.setLayoutParams(rlp);
        mImageViewPic.setWhRatio(currentData.getRatio());

        RequestOptions requestOptions = new RequestOptions()
                .placeholder(R.drawable.drawable_placeholder);

        if (currentData.isSetted()) {
            Glide.with(mContext)
                    .load(currentData.getUrls().getSmallImageUrl())
                    .into(mImageViewPic);
        } else {
            Glide.with(mContext)
                    .load(currentData.getUrls().getSmallImageUrl())
                    .transition(DrawableTransitionOptions.withCrossFade())
                    .apply(requestOptions)
                    .into(mImageViewPic);
            currentData.setSetted(true);
        }

    }

    public interface ListItemClickListener {
        void onListItemClick(int position);
    }

}
