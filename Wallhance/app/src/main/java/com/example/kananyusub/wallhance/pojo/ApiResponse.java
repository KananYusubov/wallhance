package com.example.kananyusub.wallhance.pojo;

import com.google.gson.annotations.SerializedName;

public class ApiResponse {

    @SerializedName("id")
    private String mId;
    @SerializedName("width")
    private int mWidth;
    @SerializedName("height")
    private int mHeight;
    @SerializedName("urls")
    private ImageUrl mUrls;
    private float mRatio;
    private boolean mSetted;

    public int getWidth() {
        return mWidth;
    }

    public int getHeight() {
        return mHeight;
    }

    public ImageUrl getUrls() {
        return mUrls;
    }

    public String getId() {
        return mId;
    }

    public void setRatio(float ratio) {
        mRatio = ratio;
    }

    public float getRatio() {
        return mRatio;
    }

    public void setSetted(boolean setted) {
        mSetted = setted;
    }

    public boolean isSetted() {
        return mSetted;
    }
}
