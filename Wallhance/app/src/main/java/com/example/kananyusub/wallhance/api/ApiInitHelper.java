package com.example.kananyusub.wallhance.api;

import com.example.kananyusub.wallhance.util.Constant;

import java.io.IOException;
import java.util.Collections;

import okhttp3.CipherSuite;
import okhttp3.ConnectionSpec;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.TlsVersion;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiInitHelper {
    private static Retrofit sRetrofit;
    private static OkHttpClient sOkHttpClient;
    private static ApiInitHelper mInstance;

    private ApiInitHelper() {

        if (sOkHttpClient == null) {
            initOkHttpClient();
        }

        sRetrofit = new Retrofit.Builder()
                .baseUrl(Constant.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(sOkHttpClient)
                .build();
    }

    private static void initOkHttpClient() {
        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        sOkHttpClient = new OkHttpClient.Builder()
                .addInterceptor(new Interceptor() {
                    @Override
                    public Response intercept(Chain chain) throws IOException {
                        Request originalRequest = chain.request();

                        Request newRequest = originalRequest.newBuilder()
                                .header("Accept-Version", "v1")
                                .build();
                        return chain.proceed(newRequest);
                    }
                })
                .addInterceptor(loggingInterceptor)
                .build();
    }

    public static synchronized ApiInitHelper getInstance() {
        if (mInstance == null) {
            mInstance = new ApiInitHelper();
        }

        return mInstance;
    }

    public ApiMethods getApi() {
        return sRetrofit.create(ApiMethods.class);
    }
}
