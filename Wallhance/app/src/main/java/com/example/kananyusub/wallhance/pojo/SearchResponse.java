package com.example.kananyusub.wallhance.pojo;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SearchResponse {

    @SerializedName("results")
    private List<ApiResponse> mResults;

    public List<ApiResponse> getResults() {
        return mResults;
    }
}
